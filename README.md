Mega-Monitor
============

## Overview

This is a very basic monitoring of the ambient environment system based on the Arduino framework.

Simply put, it monitors:
* ambient temperature
* relative humidity
* indoor air quality
* CO2 levels
* noise levels
* vibration levels

The functionality is enabled by an Arduino MEGA 2560 or any pin similar clone, and dedicated sensors 
such as:
* DHT11 (temperature & humidity)
* MQ135 (indoor air quality and CO2)
* microphone
* capacitive vibration sensor

A clear description of the hardware, sensor modules and general wiring can be found under the 
`hardware` directory.

## Dependencies

In order for this project to work, the following Arduino library dependencies need to be met while flashing 
the ino sketches onto the board:
* Arduino framework (tested with 1.6.9 but newer should work just fine as well)
* DHT Adafruit library available at: https://github.com/adafruit/DHT-sensor-library
* MQ135 library available at: https://github.com/rstoica/MQ135 

## CO2 gas calibration note
Prior to using the MQ135 this should be calibrated. For this purpose, feel free to use the 
`co2gas-calibration.ino` sketch. 

The steps for a proper calibration as pointed out under articles 
such as [D.Gironi-blogpost_14_01](http://davidegironi.blogspot.de/2014/01/cheap-co2-meter-using-mq135-sensor-with.html#.V41Xq_l95D-)
and [G.Krocker-log_15_01](https://hackaday.io/project/3475-sniffing-trinket/log/12363-mq135-arduino-library).
To summarize one needs to:
* let the MQ135 sensor work uninterruptedly for at least 24 hours for initial burning in and warming up 
* run the `co2gas-calibration.ino` and wait 20 minutes until the aggregate _RZero_ value of the MQ135 is computed
* set the computed _RZero_ as the sensor's _RZero_ attribute via the setter provided in the MQ135 library 

Further details may be inferred by reading the thin [datasheet](https://www.olimex.com/Products/Components/Sensors/SNS-MQ135/resources/SNS-MQ135.pdf)
of the MQ135.