/***************************************************************************************
 * 
 *  \file     co2gas-calibration.ino
 *  \author   Andrei Stoica (rstoica)
 *  \date     18.07.2016
 *  \version  0.1
 *  
 *  \brief    Arduino sketch to calibrate the MQ135 gas for CO2.
 * 
 *  The sketch leverages the MQ135 open-source library <> and the tutorial by the same
 *  author at https://hackaday.io/project/3475-sniffing-trinket/log/12363-mq135-arduino-library
 *  in calibrating the CO2 sensitivity by determining the internal RZero resistance of 
 *  the sensor in clean air. Prior to running this sketch and recording this value,
 *  leave the sensor to warm up and operate for at least 12-24 h. The calibration should 
 *  be done in clean open air, ideally at 20 deg C and 35 % relative humidity. After
 *  recording the RZero from serial monitor over COM, set it up in the mega-monitor.ino
 *  sketch.
 *  
 * ***************************************************************************************/

#include <DHT.h>
#include <MQ135.h>

#define DHTTYPE             DHT11
#define DHTPIN              2
#define MQ135PIN            97
#define CALTIME_MS          1200000

DHT dht(DHTPIN, DHTTYPE);
MQ135 mq135(MQ135PIN);

float rzeroavg = 0.0;
double iteration = 0;

void printCO2ppm(float rzero, float co2ppm) {
  Serial.print("RZero: ");
  Serial.print(rzero);
  Serial.print("kOhms\t");
  Serial.print("CO2: ");
  Serial.print(co2ppm);
  Serial.print(" ppm\t");
}

void printHumTemp(float hum, float temp, float hic){
    Serial.print("Hum: ");
    Serial.print(hum);
    Serial.print(" % RH\t");
    Serial.print("Temp: ");
    Serial.print(temp);
    Serial.print(" deg C");
    Serial.print("\t");
}

void setup() {
  Serial.begin(115200);
  Serial.println("Debug USART initialized");
}

void loop() {
  if (millis() <= CALTIME_MS)
  {
    // wait some time between the measurements
    delay(1000);
  
    float hum = dht.readHumidity();
    // read temperature directly in Celsius
    float temp = dht.readTemperature();
    float co2ppm, rzero;
  
    if (isnan(hum) || isnan(temp))
    {
      Serial.println("Failed to read information from DHT sensor"); 
  
      // if no values for temp and humidity, than go ahead and query gas without correction
      rzero = mq135.getRZero();
      co2ppm = mq135.getPPM();
    }
    else
    {
      // compute the heat index in Celsius (isFahrenheit = false) 
      float hic = dht.computeHeatIndex(temp, hum, false);
  
      // get the corrected gas CO2ppm value with recorded temp in Celsius and hum
      rzero = mq135.getRZero();
      co2ppm = mq135.getPPM();
  
      printHumTemp(hum, temp, hic);
      printCO2ppm(rzero, co2ppm);
    }
    double total_iteration = iteration + 1;
    rzeroavg = rzeroavg * (float)(iteration / total_iteration) + rzero * (float)(1 / total_iteration); 
    iteration = total_iteration;
  }
  else
  {
      Serial.println("Averaged detected RZero: ");
      Serial.println(rzeroavg);
  }
  Serial.print("\r\n");
}

