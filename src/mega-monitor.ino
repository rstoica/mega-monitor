/***************************************************************************************
 * 
 *  \file     mega-monitor.ino
 *  \author   Andrei Stoica (rstoica)
 *  \date     18.07.2016
 *  \version  0.1
 *  
 *  \brief    Monitoring utility based on Arduino MEGA
 *  
 *  This sketch monitors the ambient environment providing recordings each 2 seconds 
 *  recordings for air temperature, relative humidity, and CO2 ppm. In addition, passive
 *  continuous monitoring of the ambient noise level and local vibration is performed such
 *  that when the manually calibrated sensors trigger a signal an alarm event is signaled
 *  over the COM port. All this done is done based on the Arduino MEGA 2560 board. Ports to
 *  different Arduino platforms are possible but require user action. 
 *  
 *  The external dependencies are:
 *    * DHT Adafruit library @ https://github.com/adafruit/DHT-sensor-library
 *    * MQ135 gas sensor library @ https://github.com/GeorgK/MQ135
 * 
 *  The circuit and connectivity diagram is provided using a Fritzing sketch of the necessary
 *  components and their connections.
 *  
 * ***************************************************************************************/
 
#include <DHT.h>
#include <MQ135.h>

#define DHTTYPE             DHT11
#define DHTPIN              2
#define MQ135PIN            97
#define LEDPIN              13
#define VIBPIN              44

#define MEGA2560INTPIN3     1
#define MEGA2560INTPIN21    2
#define MEGA2560INTPIN20    3

DHT dht(DHTPIN, DHTTYPE);
MQ135 mq135(MQ135PIN);
unsigned long last_trigger_time = 0;
unsigned long last_vibration_time = 0;
unsigned long last_gas_time = 0;
float co2ppm = 0.0;

void printCO2ppm(float co2ppm) {
  Serial.print("CO2: ");
  Serial.print(co2ppm);
  Serial.print(" ppm\t");
}

void printHumTemp(float hum, float temp, float hic) {
    Serial.print("Hum: ");
    Serial.print(hum);
    Serial.print(" % RH\t");
    Serial.print("Temp: ");
    Serial.print(temp);
    Serial.print(" deg C");
    Serial.print("\t");
    Serial.print("HIC: ");
    Serial.print(hic);
    Serial.print(" deg C\t");
}

void setup() {
  Serial.begin(115200);
  Serial.println("Debug USART initialized");

  // external USART connection - use at will for different purposes (PIN18/19)
  // Serial1.begin(9600);
  // Serial1.println("Main USART initialized");
  
  // need to initialiazie the DHT driver
  dht.begin();
  Serial.println("Initialized DHT driver");
  Serial.println("Enabled debug LED output");
  
  // make the Arduino 13 LED an output and turn it ON
  pinMode(LEDPIN, OUTPUT);
  digitalWrite(LEDPIN,LOW);

  // attach interrupt to monitor continously sound levels
  attachInterrupt(MEGA2560INTPIN3,loud_sound_handler,RISING); 

  // attach interrupt to monitor continously vibration levels
  attachInterrupt(MEGA2560INTPIN20,vibration_alert_handler,FALLING);

  // attach interrupt to monitor continously gas levels
  attachInterrupt(MEGA2560INTPIN21,gas_leak_handler,FALLING);
}

void loop() {
  // wait some time between the measurements
  delay(2000);
  // digitalWrite(LEDPIN, !digitalRead(LEDPIN));

  float hum = dht.readHumidity();

  // read temperature directly in Celsius
  float temp = dht.readTemperature(); 

  if (isnan(hum) || isnan(temp))
  {
    Serial.println("Failed to read information from sensor"); 

    // if no values for temp and humidity, than go ahead and query gas without correction
    co2ppm = mq135.getPPM();
  }
  else
  {
    // compute the heat index in Celsius (isFahrenheit = false) 
    float hic = dht.computeHeatIndex(temp, hum, false);

    // get the corrected gas CO2ppm value with recorded temp in Celsius and hum 
    // this seems not to work too well - TODO-AS: Investigate!!!
    // co2ppm = mq135.getCorrectedPPM(temp, hum);
    co2ppm = mq135.getPPM();
    
    printHumTemp(hum, temp, hic);
    printCO2ppm(co2ppm);
  }
  Serial.print("\r\n");
}

void loud_sound_handler() {
  unsigned long trigger_time = millis();
  if (trigger_time - last_trigger_time > 200)
  {
    Serial.print(trigger_time);
    Serial.print("ms:\tSound alert!");
    Serial.print("!\r\n");
    digitalWrite(LEDPIN, !digitalRead(LEDPIN));
  }
  last_trigger_time = trigger_time;
}

void vibration_alert_handler() {
  unsigned long vibration_time = millis();
  if (vibration_time - last_vibration_time > 200)
  {
    Serial.print(vibration_time);
    Serial.print("ms:\tAlert vibration detected - ");
    uint8_t vibpin = digitalRead(VIBPIN);
    // Serial.print(vibpin);
    Serial.print("!\r\n");
  }
  last_vibration_time = vibration_time;
}

void gas_leak_handler() {
  unsigned long trigger_time = millis();
  if (trigger_time - last_gas_time > 500)
  {
    Serial.print(trigger_time);
    Serial.print("ms:\tCritical gas alert! Here is the CO2 level - ");
    printCO2ppm(co2ppm);
    Serial.print("\r\n");
  }
  last_gas_time = trigger_time;
}

